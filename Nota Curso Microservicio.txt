Eureka Server

Proposito:
Servicio de registro y descubrimiento

@EnableEurekaServer

Configuraciones del server:

#nombre del MS
spring.application.name=servicio-eureka-server
#puerto por defecto
server.port=8761 
#para que no se registre como cliente
eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false 

Para los clientes:

@EnableEurekaClient
eureka.client.service-url.defaultZone=http://localhost:8761/eureka

########################################################################

Zuul Api Gateway

Proposito:
Es un proxy que proporciona balanceo de carga y un punto de entrada en común
a los diferentes Microservicios, ver Patron Gateway. 

Dependencias:
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-netflix-zuul</artifactId>
</dependency>

Anotaciones Main Application

@EnableEurekaClient
@EnableZuulProxy

Configuraciones del Server application.properties:

#se registra contra en Eureka como cliente
spring.application.name=service-zuul-server
server.port=8090

eureka.client.service-url.defaultZone: http://localhost:8761/eureka

#rutas de los servicios y sus endpoints
zuul.routes.productos.service-id=service-products
zuul.routes.productos.path=/api/products/**

zuul.routes.items.service-id=servicio-items
zuul.routes.items.path=/api/items/**

Zuul proporciona 4 filtros básicos que nos permiten interceptar el tráfico en diferentes momentos del procesado de una petición.

pre filters — Este filtro es invocado antes de realizar el enrutamiento de la petición. Se define Programaticamente
post filters — Invocado después del enrutamiento de la petición. Se define Programaticamente
route filters — Filtro utilizado para realizar el enrutamiento.
error filters — Invocado cuando ocurre un error en el procesado de la petición por parte de Zuul.

######################################################

Spring Cloud configuration Server

Proposito:
Servidor de configuraciones de ambiente (Enviroment) 

Requisitos:
Fichero de configuraciones bootstrap.properties en el cliente
este sobreescribirá la configuracion actual o la configuracion por defecto

Dependencia:
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-config-server</artifactId>
</dependency>

Anotaciones en el Main:

@EnableConfigServer

Configuracion del Server application.properties:

spring.application.name=config-server
server.port:8888

#fichero local de configuraciones
spring.cloud.config.server.git.uri=file:///C:/Users/Ruben/msconfig

Configuracion de Cliente ejemplo:

<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-config</artifactId>
</dependency>

bootstrap.properties:

spring.application.name=servicio-items
spring.profiles.active=produccion
spring.cloud.config.uri=http://localhost:8888

Archivos de configuracion de ambiente en bootstrap.properties para el cliente:

nombreservicio-nombreambiente.properties

Ej:

servicio-items.properties
servicio-items-desarrollo.properties
servicio-items-produccion.properties

Como apuntar a un ambiente desde el cliente:

spring.profiles.active=nombreambiente
spring.cloud.config.uri=http://localhost:8888






